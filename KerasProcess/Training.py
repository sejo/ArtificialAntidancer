import numpy as np

a=np.loadtxt("data/dataD.txt",delimiter=',')



print(np.shape(a))
b = []
A = []
for line in a:
    for i in line:
        b.append(i)
    A.append(b)
    b = []
    
print(np.shape(A))


from keras.callbacks import ModelCheckpoint
from keras.models import Sequential
from keras.layers import LSTM, Dense
import numpy as np

data_dim = 75
timesteps = 2
num_classes = 75

# expected input data shape: (batch_size, timesteps, data_dim)
model = Sequential()
model.add(LSTM(data_dim*2, return_sequences=True,
               input_shape=(timesteps, data_dim)))
model.add(LSTM(data_dim*2, return_sequences=True))
model.add(LSTM(data_dim*2))
# Changed activation function
model.add(Dense(num_classes, activation='sigmoid'))

# Changed loss
model.compile(loss='mean_squared_error',
              optimizer='rmsprop',
              metrics=['accuracy'])


nums_x = []
nums_y = []

N = len(A)

for D in range(N-30,N):
    print("D:",D)
    for i in range(0,N-D):
        for a in range(0,D):
            nums_x.append([A[i+a],A[i+D]])
            nums_y.append(A[i+a+1])
    



x_train = np.array(nums_x)
y_train = np.array(nums_y)


checkpointCallback = ModelCheckpoint('weights.{epoch:02d}.hdf5', monitor='val_loss', verbose=0, save_best_only=False, save_weights_only=False, mode='auto', period=1)

model.fit(x_train, y_train, batch_size=data_dim*4, epochs=20, callbacks=[checkpointCallback])




model.save('model.h5')
