import java.util.Arrays;

class SkeletonVector{
	 int N_JOINTS = 25; // Number of joints
	 int N_DIRS = 24; // Number of direction vectors
	 int N_DIMS = 3;

	float [] spn; // Skeleton Positions as Numbers
	PVector[] sdv; // Skeleton Directions as Vectors
	float [] sdn; // Skeleton Directions as Numbers

	SkeletonVector(){

		// Create data structures

		spn = new float[N_JOINTS*N_DIMS];

		sdv = new PVector[N_DIRS];

		sdn = new float[N_JOINTS*N_DIMS];

		for(int i=0; i<N_DIRS;i++){
			sdv[i] = new PVector();
		}

	}

	public void updateWithJoints(KJoint [] joints){

		// Skeleton Position Numbers
		calculateSPN(joints);

		// Skeleton Directions Vectors
		calculateSDV(joints);

		// Skeleton Directions Numbers
		calculateSDN();



	}

	public String getStringSDN(){
		return getStringFromNumbers(sdn);
	}

	public String getStringSPN(){
		return getStringFromNumbers(spn);
	}

	private String getStringFromNumbers(float [] n){
		String str = Arrays.toString(n);
		// Remove trailing [ ]
		return str.substring(1,str.length()-1);
	}


	// Read the joints as a list of numbers
	// Skeleton Position Numbers
	private void calculateSPN(KJoint[] joints){
		KJoint j;
		int a;
    
		for(int i=0; i<N_JOINTS; i++){
			j = joints[i];
			a = i*N_DIMS; // spn starting index

			spn[a] = j.getX();
			spn[a+1] = j.getY();
			spn[a+2] = j.getZ();
		}
	}

	// Calculate the unit vector for each bone
	private void calculateSDV(KJoint[] joints){
		sdv[0].set(unitBone(joints, KinectPV2.JointType_Head, KinectPV2.JointType_Neck));
		sdv[1].set(unitBone(joints, KinectPV2.JointType_Neck, KinectPV2.JointType_SpineShoulder));
		sdv[2].set(unitBone(joints, KinectPV2.JointType_SpineShoulder, KinectPV2.JointType_SpineMid));
		sdv[3].set(unitBone(joints, KinectPV2.JointType_SpineMid, KinectPV2.JointType_SpineBase));
		sdv[4].set(unitBone(joints, KinectPV2.JointType_SpineShoulder, KinectPV2.JointType_ShoulderRight));
		sdv[5].set(unitBone(joints, KinectPV2.JointType_SpineShoulder, KinectPV2.JointType_ShoulderLeft));
		sdv[6].set(unitBone(joints, KinectPV2.JointType_SpineBase, KinectPV2.JointType_HipRight));
		sdv[7].set(unitBone(joints, KinectPV2.JointType_SpineBase, KinectPV2.JointType_HipLeft));

// Right Arm    
		sdv[8].set(unitBone(joints, KinectPV2.JointType_ShoulderRight, KinectPV2.JointType_ElbowRight));
		sdv[9].set(unitBone(joints, KinectPV2.JointType_ElbowRight, KinectPV2.JointType_WristRight));
		sdv[10].set(unitBone(joints, KinectPV2.JointType_WristRight, KinectPV2.JointType_HandRight));
		sdv[11].set(unitBone(joints, KinectPV2.JointType_HandRight, KinectPV2.JointType_HandTipRight));
		sdv[12].set(unitBone(joints, KinectPV2.JointType_WristRight, KinectPV2.JointType_ThumbRight));

// Left Arm
		sdv[13].set(unitBone(joints, KinectPV2.JointType_ShoulderLeft, KinectPV2.JointType_ElbowLeft));
		sdv[14].set(unitBone(joints, KinectPV2.JointType_ElbowLeft, KinectPV2.JointType_WristLeft));
		sdv[15].set(unitBone(joints, KinectPV2.JointType_WristLeft, KinectPV2.JointType_HandLeft));
		sdv[16].set(unitBone(joints, KinectPV2.JointType_HandLeft, KinectPV2.JointType_HandTipLeft));
		sdv[17].set(unitBone(joints, KinectPV2.JointType_WristLeft, KinectPV2.JointType_ThumbLeft));

// Right Leg
		sdv[18].set(unitBone(joints, KinectPV2.JointType_HipRight, KinectPV2.JointType_KneeRight));
		sdv[19].set(unitBone(joints, KinectPV2.JointType_KneeRight, KinectPV2.JointType_AnkleRight));
		sdv[20].set(unitBone(joints, KinectPV2.JointType_AnkleRight, KinectPV2.JointType_FootRight));

// Left Leg
		sdv[21].set(unitBone(joints, KinectPV2.JointType_HipLeft, KinectPV2.JointType_KneeLeft));
		sdv[22].set(unitBone(joints, KinectPV2.JointType_KneeLeft, KinectPV2.JointType_AnkleLeft));
		sdv[23].set(unitBone(joints, KinectPV2.JointType_AnkleLeft, KinectPV2.JointType_FootLeft));
		
	
	}

	// Calculate the unit vector between two joints
	private PVector unitBone(KJoint[] joints, int jointType1, int jointType2){
		// Create vectors
		PVector A = new PVector(joints[jointType1].getX(),joints[jointType1].getY(),joints[jointType1].getZ());
		PVector B = new PVector(joints[jointType2].getX(),joints[jointType2].getY(),joints[jointType2].getZ());

		// Substract them
		PVector d = PVector.sub(B,A);

		// Convert to unit vector
		d.normalize();

		return d;
	}


	// Calculate the 
	// Skeleton Directions as Numbers (Normalized)
	private void calculateSDN(){

		int a;

		float ssum = 0; 

    int i = 0;
		for(PVector v : sdv){
			a = i*N_DIMS;

			sdn[a] = v.x;
			sdn[a+1] = v.y;
			sdn[a+2] = v.z;

			ssum += sq(v.x);
			ssum += sq(v.y);
			ssum += sq(v.z);
      i++;
		}

		float mag = sqrt(ssum);

		// Normalize the whole vector
		for( i=0; i<sdn.length; i++){
			sdn[i] /= mag;
		}

	}




}