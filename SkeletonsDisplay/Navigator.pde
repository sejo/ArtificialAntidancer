class Navigator{
	float [][] nums;
	float [][] distances;
	Node [] nodes;	
	int N;
	int D;

	float t,tinc;
	int i1,i2;
	float [] start;
	float [] target;


	float [] hum;

	int recPtr;

	Navigator(int numNodes){
		N = numNodes;
		distances = new float[N][N];
		nodes = new Node[N];

		t = 0;
		tinc = 0.1;

		recPtr = 0;

		i1 = int(random(N));
		i2 = int(random(N));


	}

	public void setNums(float [][] n){
		nums = n;
		D = nums[0].length;


		hum = new float[D];
		
		start = nums[i1];
		target = nums[i2];
	}

	public void createGraph(int k){
		float [] neighs;
		float [] sorted;
		boolean found;
		for(int i=0; i<N; i++){
			nodes[i] = new Node(i);

			neighs = distances[i];
			sorted = sort(neighs);

			// Search for the kNN (excluding the first element that is itself)
			for(int n=1; n<=k; n++){
				found = false;
				for(int j=0; j<sorted.length && !found && nodes[i].vertices.size()<k;j++){
					if(neighs[j] == sorted[n]){
						nodes[i].addVertex(j,neighs[j]);
					}
				}
			}
		}


	}

	public void updateRecPtr(int p){
		recPtr = p;
	}

	public void updateHum(float [] h){
		arrayCopy(h,hum);
	}

	public float[] getNext(){

		float[] n = new float[D];

		/*
		for(int i=0; i<D;i++){
			n[i] = nums[0][i];
		}
		*/




		if(t>=1){
			if(random(1)<0.5){
				i1 = i2;
				start = nums[i1];
				i2 = getCounterpointedIndex();
				target = nums[i2];
			}
			else{
				println("sample");
				arrayCopy(target,start);
				arrayCopy(hum,target);
			}
			t = 0;
			tinc = random(0.01,0.2);
		}

		n = nlerp(start,target,t);

		t+=tinc;

		return n;
	}

	private int getCounterpointedIndex(){
		float r = random(1);
		if(r<0.2){
			println("random");
			return int(random(N));
		}
		else if(r<0.7){
			println("rec");
			return recPtr;
		}
		else{
			println("closest");
			return getClosest(hum);
		}
	}

	public float[] nlerp(float[] n1, float[] n2, float val){
		float [] n = new float[D];
		for(int i=0; i<D; i++){
			n[i] = lerp(n1[i],n2[i],val);
		}
		return n;

	}

	public void calculateDistances(float[][] n){
		float [] n1;
		float [] n2;
		float d;

		for(int i=0; i<N; i++){
			n1 = n[i];
			for(int j=0; j<N; j++){
				n2 = n[j];
				distances[i][j] = distance(n1,n2);
			}
		}
	}

	// n1 and n2 are multimendsional vectors
	float distance(float[] n1, float[] n2){
		float sum = 0;
		for(int i=0; i<n1.length; i++){
			sum += sq(n1[i]-n2[i]);
		}
		return sqrt(sum);
	}

	int getClosest(float[] n1){
		float m = 1.0;
		int mIndex = 0;
		float d;
		float [] n2;
		for(int i=0; i<N; i++){
			n2 = nums[i];
			d = distance(n1,n2);
			if(d<m){
				m = d;
				mIndex = i;
			}

		}
		return mIndex;
	}




}


class Node{
	ArrayList<Vertex> vertices;
	int index;

	Node(int i){
		index = i;
		vertices = new ArrayList<Vertex>();
	}

	void addVertex(int pointer, float cost){
		if(!contains(pointer)){
			vertices.add(new Vertex(pointer,cost));
		}
	}

	boolean contains(int pointer){
		for(int i=0; i<vertices.size(); i++){
			if(vertices.get(i).pointer == pointer){
				return true;
			}
		}
		return false;

	}

	String verticesString(){
		Vertex v;
		String str = "";
		for(int i=0; i<vertices.size(); i++){
			v = vertices.get(i);
			str+=v.pointer+": "+v.cost+"\t";
		}
		return str;
	}

}

class Vertex{
	int pointer;
	float cost;

	Vertex(int p, float c){
		pointer = p;
		cost = c;
	}

}