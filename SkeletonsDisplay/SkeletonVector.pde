import KinectPV2.*;
import java.util.Arrays;

class SkeletonVector{
	 int N_JOINTS = 25; // Number of joints
	 int N_DIRS = 24; // Number of direction vectors
	 int N_DIMS = 3;

	PVector[] spv; // Skeleton Positions as Vectors
	float [] spn; // Skeleton Positions as Numbers
	PVector[] sdv; // Skeleton Directions as Vectors
	float [] sdn; // Skeleton Directions as Numbers

	SkeletonVector(){

		// Create data structures

		spv = new PVector[N_JOINTS];

		for(int i=0; i<N_JOINTS; i++){
			spv[i] = new PVector();
		}

		spn = new float[N_JOINTS*N_DIMS];

		sdv = new PVector[N_DIRS];

		sdn = new float[N_JOINTS*N_DIMS];

		for(int i=0; i<N_DIRS;i++){
			sdv[i] = new PVector();
		}

	}

	public void updateWithJoints(KJoint [] joints){

		// Skeleton Position Numbers
		calculateSPN(joints);

		// Skeleton Directions Vectors
		calculateSDV(joints);

		// Skeleton Directions Numbers
		calculateSDN();
	}

	public PVector [] getSPV(){
		return spv;
	}

	public PVector [] getSDV(){
		return sdv;
	}

	public float [] getSDN(){
		return sdn;
	}



	public String getStringSDN(){
		return getStringFromNumbers(sdn);
	}

	public String getStringSPN(){
		return getStringFromNumbers(spn);
	}

	private String getStringFromNumbers(float [] n){
		String str = Arrays.toString(n);
		// Remove trailing [ ]
		return str.substring(1,str.length()-1);
	}


	// Read the joints as a list of numbers
	// Skeleton Position Numbers
	private void calculateSPN(KJoint[] joints){
		KJoint j;
		int a;
    
		for(int i=0; i<N_JOINTS; i++){
			j = joints[i];
			a = i*N_DIMS; // spn starting index

			spn[a] = j.getX();
			spn[a+1] = j.getY();
			spn[a+2] = j.getZ();
		}
	}



	// Calculate the unit vector for each bone
	private void calculateSDV(KJoint[] joints){
		sdv[0].set(unitBone(joints, KinectPV2.JointType_Head, KinectPV2.JointType_Neck));
		sdv[1].set(unitBone(joints, KinectPV2.JointType_Neck, KinectPV2.JointType_SpineShoulder));
		sdv[2].set(unitBone(joints, KinectPV2.JointType_SpineShoulder, KinectPV2.JointType_SpineMid));
		sdv[3].set(unitBone(joints, KinectPV2.JointType_SpineMid, KinectPV2.JointType_SpineBase));
		sdv[4].set(unitBone(joints, KinectPV2.JointType_SpineShoulder, KinectPV2.JointType_ShoulderRight));
		sdv[5].set(unitBone(joints, KinectPV2.JointType_SpineShoulder, KinectPV2.JointType_ShoulderLeft));
		sdv[6].set(unitBone(joints, KinectPV2.JointType_SpineBase, KinectPV2.JointType_HipRight));
		sdv[7].set(unitBone(joints, KinectPV2.JointType_SpineBase, KinectPV2.JointType_HipLeft));

// Right Arm    
		sdv[8].set(unitBone(joints, KinectPV2.JointType_ShoulderRight, KinectPV2.JointType_ElbowRight));
		sdv[9].set(unitBone(joints, KinectPV2.JointType_ElbowRight, KinectPV2.JointType_WristRight));
		sdv[10].set(unitBone(joints, KinectPV2.JointType_WristRight, KinectPV2.JointType_HandRight));
		sdv[11].set(unitBone(joints, KinectPV2.JointType_HandRight, KinectPV2.JointType_HandTipRight));
		sdv[12].set(unitBone(joints, KinectPV2.JointType_WristRight, KinectPV2.JointType_ThumbRight));

// Left Arm
		sdv[13].set(unitBone(joints, KinectPV2.JointType_ShoulderLeft, KinectPV2.JointType_ElbowLeft));
		sdv[14].set(unitBone(joints, KinectPV2.JointType_ElbowLeft, KinectPV2.JointType_WristLeft));
		sdv[15].set(unitBone(joints, KinectPV2.JointType_WristLeft, KinectPV2.JointType_HandLeft));
		sdv[16].set(unitBone(joints, KinectPV2.JointType_HandLeft, KinectPV2.JointType_HandTipLeft));
		sdv[17].set(unitBone(joints, KinectPV2.JointType_WristLeft, KinectPV2.JointType_ThumbLeft));

// Right Leg
		sdv[18].set(unitBone(joints, KinectPV2.JointType_HipRight, KinectPV2.JointType_KneeRight));
		sdv[19].set(unitBone(joints, KinectPV2.JointType_KneeRight, KinectPV2.JointType_AnkleRight));
		sdv[20].set(unitBone(joints, KinectPV2.JointType_AnkleRight, KinectPV2.JointType_FootRight));

// Left Leg
		sdv[21].set(unitBone(joints, KinectPV2.JointType_HipLeft, KinectPV2.JointType_KneeLeft));
		sdv[22].set(unitBone(joints, KinectPV2.JointType_KneeLeft, KinectPV2.JointType_AnkleLeft));
		sdv[23].set(unitBone(joints, KinectPV2.JointType_AnkleLeft, KinectPV2.JointType_FootLeft));
		
	
	}

	// Calculate the unit vector between two joints
	private PVector unitBone(KJoint[] joints, int jointType1, int jointType2){
		// Create vectors
		PVector A = new PVector(joints[jointType1].getX(),joints[jointType1].getY(),joints[jointType1].getZ());
		PVector B = new PVector(joints[jointType2].getX(),joints[jointType2].getY(),joints[jointType2].getZ());

		// Substract them
		PVector d = PVector.sub(B,A);

		// Convert to unit vector
		d.normalize();

		return d;
	}


	// Calculate the 
	// Skeleton Directions as Numbers (Normalized)
	private void calculateSDN(){

		int a;

		float ssum = 0; 

    int i = 0;
		for(PVector v : sdv){
			a = i*N_DIMS;

			sdn[a] = v.x;
			sdn[a+1] = v.y;
			sdn[a+2] = v.z;

			ssum += sq(v.x);
			ssum += sq(v.y);
			ssum += sq(v.z);
      i++;
		}

		float mag = sqrt(ssum);

		// Normalize the whole vector
		for( i=0; i<sdn.length; i++){
			sdn[i] /= mag;
		}
	}





	public void calculateSDVfromSDN(float [] n){
		for(int i=0; i<N_DIRS; i++){
			sdv[i].set(n[i*N_DIMS],n[i*N_DIMS+1],n[i*N_DIMS+2]);
			sdv[i].normalize();
		}
		sdn = n;
	}

	public void calculateSPVfromSPN(float [] n){
		for(int i=0; i<N_JOINTS; i++){
			spv[i].set(n[i*N_DIMS],n[i*N_DIMS+1],n[i*N_DIMS+2]);
		}

	}


	public void drawSDVwithSPV(){
		drawSDVwithSPV(sdv, spv);
	}

	public void drawSDVwithSPV(PVector [] sd, PVector [] sp){
		PVector [] newsp = new PVector[N_JOINTS];
		PVector [] scaledSD = new PVector[N_DIRS];

		// Get magnitudes of the bones of the example pose
		float [] mags = getBonesMag(sp);

		// Scale the direction vectors to those magnitudes
		for(int i=0; i<N_DIRS; i++){
			scaledSD[i] = new PVector();
			scaledSD[i].set(sd[i]);
			scaledSD[i].mult(mags[i]);
		}

		for(int i=0; i<N_JOINTS; i++){
			newsp[i] = new PVector();
		}

		PVector head = new PVector();
		PVector top = new PVector();
		PVector mid = new PVector();
		PVector base = new PVector();
		PVector aux = new PVector();
		PVector aux2 = new PVector();

		// Start from SpineMid
		head.set(sp[KinectPV2.JointType_Head]);
		newsp[KinectPV2.JointType_Head].set(head);

		top.set(head);
		top.add(scaledSD[0]); // Head to neck
		newsp[KinectPV2.JointType_Neck].set(top);

		top.add(scaledSD[1]); // Neck to spine shoulder
		newsp[KinectPV2.JointType_SpineShoulder].set(top);

		mid.set(top);
		mid.add(scaledSD[2]); // Spine Shoulder to Mid
		newsp[KinectPV2.JointType_SpineMid].set(mid);

		base.set(mid);
		base.add(scaledSD[3]); // Mid to Base

		newsp[KinectPV2.JointType_SpineBase].set(base);

		// RIGHT LEG
		aux.set(base);
		aux.add(scaledSD[6]); // Base to Hip Right

		newsp[KinectPV2.JointType_HipRight].set(aux);

		aux.add(scaledSD[18]); // Hip Right to Knee Right
		newsp[KinectPV2.JointType_KneeRight].set(aux);

		aux.add(scaledSD[19]);// Knee Right to Ankle right
		newsp[KinectPV2.JointType_AnkleRight].set(aux);

		aux.add(scaledSD[20]);// Ankle right to foot right
		newsp[KinectPV2.JointType_FootRight].set(aux);

		// LEFT LEG
		aux.set(base);
		aux.add(scaledSD[7]); // Base to Hip Left 

		newsp[KinectPV2.JointType_HipLeft].set(aux);

		aux.add(scaledSD[21]); // Hip Left to Knee Left
		newsp[KinectPV2.JointType_KneeLeft].set(aux);

		aux.add(scaledSD[22]);// Knee Left to Ankle left
		newsp[KinectPV2.JointType_AnkleLeft].set(aux);

		aux.add(scaledSD[23]);// Ankle left to foot left
		newsp[KinectPV2.JointType_FootLeft].set(aux);

		// Right arm
		aux.set(top);
		aux.add(scaledSD[4]); // to Shoulder right
		newsp[KinectPV2.JointType_ShoulderRight].set(aux);

		aux.add(scaledSD[8]);// to elbow right
		newsp[KinectPV2.JointType_ElbowRight].set(aux);

		aux.add(scaledSD[9]);// to wrist right
		newsp[KinectPV2.JointType_WristRight].set(aux);

		aux2.set(aux);

		aux2.add(scaledSD[10]);// to Hand right
		newsp[KinectPV2.JointType_HandRight].set(aux2);

		aux2.add(scaledSD[11]);// To hand tip right
		newsp[KinectPV2.JointType_HandTipRight].set(aux2);

		aux.add(scaledSD[12]);// to thumb right
		newsp[KinectPV2.JointType_ThumbRight].set(aux);


		// Left arm
		aux.set(top);
		aux.add(scaledSD[5]); // to Shoulder left
		newsp[KinectPV2.JointType_ShoulderLeft].set(aux);

		aux.add(scaledSD[13]);// to elbow left
		newsp[KinectPV2.JointType_ElbowLeft].set(aux);

		aux.add(scaledSD[14]);// to wrist left
		newsp[KinectPV2.JointType_WristLeft].set(aux);

		aux2.set(aux);

		aux2.add(scaledSD[15]);// to Hand left
		newsp[KinectPV2.JointType_HandLeft].set(aux2);

		aux2.add(scaledSD[16]);// To hand tip left
		newsp[KinectPV2.JointType_HandTipLeft].set(aux2);

		aux.add(scaledSD[17]);// to thumb left
		newsp[KinectPV2.JointType_ThumbLeft].set(aux);


		drawBody(newsp);


	}

	private float getBoneMag(PVector[] sp, int jointType1, int jointType2){
		PVector v = PVector.sub(sp[jointType2],sp[jointType1]); 
		return v.mag();
	}

	private float[] getBonesMag(PVector[] sp){
		float [] mags = new float[N_DIRS];

		mags[0] = getBoneMag(sp, KinectPV2.JointType_Head, KinectPV2.JointType_Neck);
		mags[1] = getBoneMag(sp, KinectPV2.JointType_Neck, KinectPV2.JointType_SpineShoulder);
		mags[2] = getBoneMag(sp, KinectPV2.JointType_SpineShoulder, KinectPV2.JointType_SpineMid);
		mags[3] = getBoneMag(sp, KinectPV2.JointType_SpineMid, KinectPV2.JointType_SpineBase);
		mags[4] = getBoneMag(sp, KinectPV2.JointType_SpineShoulder, KinectPV2.JointType_ShoulderRight);
		mags[5] = getBoneMag(sp, KinectPV2.JointType_SpineShoulder, KinectPV2.JointType_ShoulderLeft);
		mags[6] = getBoneMag(sp, KinectPV2.JointType_SpineBase, KinectPV2.JointType_HipRight);
		mags[7] = getBoneMag(sp, KinectPV2.JointType_SpineBase, KinectPV2.JointType_HipLeft);

// Right Arm    
		mags[8] = getBoneMag(sp, KinectPV2.JointType_ShoulderRight, KinectPV2.JointType_ElbowRight);
		mags[9] = getBoneMag(sp, KinectPV2.JointType_ElbowRight, KinectPV2.JointType_WristRight);
		mags[10] = getBoneMag(sp, KinectPV2.JointType_WristRight, KinectPV2.JointType_HandRight);
		mags[11] = getBoneMag(sp, KinectPV2.JointType_HandRight, KinectPV2.JointType_HandTipRight);
		mags[12] = getBoneMag(sp, KinectPV2.JointType_WristRight, KinectPV2.JointType_ThumbRight);

// Left Arm
		mags[13] = getBoneMag(sp, KinectPV2.JointType_ShoulderLeft, KinectPV2.JointType_ElbowLeft);
		mags[14] = getBoneMag(sp, KinectPV2.JointType_ElbowLeft, KinectPV2.JointType_WristLeft);
		mags[15] = getBoneMag(sp, KinectPV2.JointType_WristLeft, KinectPV2.JointType_HandLeft);
		mags[16] = getBoneMag(sp, KinectPV2.JointType_HandLeft, KinectPV2.JointType_HandTipLeft);
		mags[17] = getBoneMag(sp, KinectPV2.JointType_WristLeft, KinectPV2.JointType_ThumbLeft);

// Right Leg
		mags[18] = getBoneMag(sp, KinectPV2.JointType_HipRight, KinectPV2.JointType_KneeRight);
		mags[19] = getBoneMag(sp, KinectPV2.JointType_KneeRight, KinectPV2.JointType_AnkleRight);
		mags[20] = getBoneMag(sp, KinectPV2.JointType_AnkleRight, KinectPV2.JointType_FootRight);

// Left Leg
		mags[21] = getBoneMag(sp, KinectPV2.JointType_HipLeft, KinectPV2.JointType_KneeLeft);
		mags[22] = getBoneMag(sp, KinectPV2.JointType_KneeLeft, KinectPV2.JointType_AnkleLeft);
		mags[23] = getBoneMag(sp, KinectPV2.JointType_AnkleLeft, KinectPV2.JointType_FootLeft);

		return mags;


	}


	private float getLowestY(PVector[] sp){
		float m = 1.0;
		for(int i=0; i<N_JOINTS; i++){
			if(sp[i].y < m){
				m = sp[i].y;
			}
		}
		return m;
	}

	private float getYDisplacement(PVector [] sp){
		float low = getLowestY(sp);
		float head = sp[KinectPV2.JointType_Head].y;

		return head-low;

	}

	//Based in  Thomas Sanchez Lengeling example
void drawBody(PVector[] sp) {
	pushMatrix();


	rotateX(PI);

	translate(0,getYDisplacement(sp));

	translate(0,-2);


  drawBall(sp, KinectPV2.JointType_Head, KinectPV2.JointType_Neck);
  drawBone(sp, KinectPV2.JointType_Neck, KinectPV2.JointType_SpineShoulder);
  drawBone(sp, KinectPV2.JointType_SpineShoulder, KinectPV2.JointType_SpineMid);

  drawBone(sp, KinectPV2.JointType_SpineMid, KinectPV2.JointType_SpineBase);
  
  drawBone(sp, KinectPV2.JointType_SpineShoulder, KinectPV2.JointType_ShoulderRight);
  drawBone(sp, KinectPV2.JointType_SpineShoulder, KinectPV2.JointType_ShoulderLeft);
  drawBone(sp, KinectPV2.JointType_SpineBase, KinectPV2.JointType_HipRight);
  drawBone(sp, KinectPV2.JointType_SpineBase, KinectPV2.JointType_HipLeft);

  // Right Arm    
  drawBone(sp, KinectPV2.JointType_ShoulderRight, KinectPV2.JointType_ElbowRight);
  drawBone(sp, KinectPV2.JointType_ElbowRight, KinectPV2.JointType_WristRight);
  drawBone(sp, KinectPV2.JointType_WristRight, KinectPV2.JointType_HandRight);
  drawBone(sp, KinectPV2.JointType_HandRight, KinectPV2.JointType_HandTipRight);
  drawBone(sp, KinectPV2.JointType_WristRight, KinectPV2.JointType_ThumbRight);

  // Left Arm
  drawBone(sp, KinectPV2.JointType_ShoulderLeft, KinectPV2.JointType_ElbowLeft);
  drawBone(sp, KinectPV2.JointType_ElbowLeft, KinectPV2.JointType_WristLeft);
  drawBone(sp, KinectPV2.JointType_WristLeft, KinectPV2.JointType_HandLeft);
  drawBone(sp, KinectPV2.JointType_HandLeft, KinectPV2.JointType_HandTipLeft);
  drawBone(sp, KinectPV2.JointType_WristLeft, KinectPV2.JointType_ThumbLeft);

  // Right Leg
  drawBone(sp, KinectPV2.JointType_HipRight, KinectPV2.JointType_KneeRight);
  drawBone(sp, KinectPV2.JointType_KneeRight, KinectPV2.JointType_AnkleRight);
  drawBone(sp, KinectPV2.JointType_AnkleRight, KinectPV2.JointType_FootRight);

  // Left Leg
  drawBone(sp, KinectPV2.JointType_HipLeft, KinectPV2.JointType_KneeLeft);
  drawBone(sp, KinectPV2.JointType_KneeLeft, KinectPV2.JointType_AnkleLeft);
  drawBone(sp, KinectPV2.JointType_AnkleLeft, KinectPV2.JointType_FootLeft);

  popMatrix();

}
void drawBone(PVector[] sp, int jointType1, int jointType2) {
//  strokeWeight(2.0f + sp[jointType1].z*8);
	strokeWeight(0.01);
  beginShape(LINES);
  /* 2D
  vertex(sp[jointType1].x, sp[jointType1].y);
  vertex(sp[jointType2].x, sp[jointType2].y);
  */
  // 3D
  vertex(sp[jointType1].x, sp[jointType1].y, sp[jointType1].z);
  vertex(sp[jointType2].x, sp[jointType2].y, sp[jointType2].z);
  endShape();
}

void drawBall(PVector[] sp, int jointType1, int jointType2) {
	strokeWeight(0.01);
	noFill();
	PVector dif = PVector.sub(sp[jointType1],sp[jointType2]);
	float r = dif.mag();

	pushMatrix();
	translate(sp[jointType1].x, sp[jointType1].y, sp[jointType1].z);
	ellipse(0,0,r*2,r*2);
	popMatrix();
  }




}
