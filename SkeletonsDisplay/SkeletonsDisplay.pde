import KinectPV2.*;

Navigator nav;

SkeletonVector skeVecRec;
SkeletonVector skeVecArt;
SkeletonVector skeVecHum;

float [][] nums;

float [] pos;

int N;
int ptr = 0;

boolean recording;
int recordedFrames;

// KINECT
KinectPV2 kinect;

void setup(){
	frameRate(30);
//  fullScreen(P3D);
	size(800,600,P3D);

	skeVecRec = new SkeletonVector();
	skeVecArt = new SkeletonVector();
	skeVecHum = new SkeletonVector();

	// Load SDN
	String [] lines = loadStrings("data/dataD.txt");
//	String [] lines = loadStrings("data/out5.txt");
	N = lines.length;

	
	nums = new float[lines.length][75];
	String [] tokens;
	for(int i=0; i<lines.length; i++){
		tokens = split(lines[i],',');

		for(int j=0; j<tokens.length; j++){
			nums[i][j] = float(tokens[j]);
		}
	}


	println("Calculating distances...");
	nav = new Navigator(N);
	nav.setNums(nums);
	nav.calculateDistances(nums);

	println("Creating graph...");
	nav.createGraph(50);


	println("Configuring skeletons...");
	// Load SPN
	lines = loadStrings("data/dataP_200.txt");

	pos = new float[75];
	tokens = split(lines[0],',');
	for(int i=0; i<pos.length; i++){
		pos[i] = float(tokens[i]);
	}
	skeVecRec.calculateSPVfromSPN(pos);
	skeVecArt.calculateSPVfromSPN(pos);
	skeVecHum.calculateSPVfromSPN(pos);


	// KINECT
	println("Initializing kinect...");
	kinect = new KinectPV2(this);
	kinect.enableSkeleton3DMap(true);
	kinect.init();


  recording = false;
  recordedFrames = 0;
	println("Starting!");
	

}

void draw(){
	background(0);
	
	// KINECT
	ArrayList<KSkeleton> skeletonArray =  kinect.getSkeleton3d();    
	if(skeletonArray.size()>0){
		KSkeleton skeleton = (KSkeleton) skeletonArray.get(0);
		if(skeleton.isTracked()){
			KJoint[] joints = skeleton.getJoints();
			skeVecHum.updateWithJoints(joints);
      nav.updateHum(skeVecHum.getSDN());
		}
	}



	// Update recorded skeleton
	skeVecRec.calculateSDVfromSDN(nums[ptr]);

	

	// Update navigator
	nav.updateRecPtr(ptr);


	// Get next pose for artificial
	skeVecArt.calculateSDVfromSDN(nav.getNext());



	translate(width/2,height/2);
	stroke(255);

	scale(300);



	// Draw Recording
	stroke(100,100,255);
	pushMatrix();
	translate(1,0);
	skeVecRec.drawSDVwithSPV();
	popMatrix();

	// Draw art
	stroke(255,255,100);
	skeVecArt.drawSDVwithSPV();

	// Draw Human
	stroke(100,255,100);
	pushMatrix();
	translate(-1,0);
	skeVecHum.drawSDVwithSPV();
	popMatrix();




	// Increase Rec pointer
	ptr = (ptr+1)%N;

	
	if(recording){
    if(recordedFrames<3  000){
	    save(String.format("frame-%04d.png",recordedFrames));
      recordedFrames++;
    }
    else{
      println("Finished recording!");
      recording = false;
      recordedFrames = 0;
    //  exit();
    }
	}

}

void keyPressed(){
  switch(key){
   case 'r':
     recording = true;
   break;
  }
}